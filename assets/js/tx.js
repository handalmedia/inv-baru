$(function(){

	var base = $("#base").val();
	$.ajaxSetup({
		type:"POST",
		url: base+"/penjualan/ambil_data",
		cache: false,
	});
		// Element 0
		$("#barang_id0").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga0").val(respond);								
							$("#qty0").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok0").val(respond);
					}
				})
			}
		});

		$("#qty0").keyup(function(){
			var stok = $("#stok0").val();
			var qty  = $(this).val();
				var jml = $("#harga0").val() * $("#qty0").val();
				$("#sub0").val(jml);
				
				
				var total = 0;
				for (var i = 0; i < 20; i++) {
					if($("#sub"+i).val())
					total = total + parseInt($("#sub"+i).val());
				}			
				$("#total").val(total);

				$.ajax({
					data:{mod:"sub",id:jml},
						success: function(respond){
								$("#jml0").html(respond);
						}
				});
				$.ajax({
					data:{mod:"sub",id:total},
						success: function(res){
								$("#showTotal").html(res);
						}
				});

		});

		// End Element 0

		// Element 1
		$("#barang_id1").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga1").val(respond);								
							$("#qty1").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok1").val(respond);
					}
				})
			}
		});

		$("#qty1").keyup(function(){
			var stok = $("#stok1").val();
			var qty  = $(this).val();
			
			var jml = $("#harga1").val() * $("#qty1").val();
			$("#sub1").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml1").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 1
		// Element 2
		$("#barang_id2").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga2").val(respond);								
							$("#qty2").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok2").val(respond);
					}
				})
			}
		});

		$("#qty2").keyup(function(){
			var stok = $("#stok2").val();
			var qty  = $(this).val();
			
			var jml = $("#harga2").val() * $("#qty2").val();
			$("#sub2").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml2").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTota2").html(res);
					}
			});
		});

		// End Element 2

		// Element 3
		$("#barang_id3").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga3").val(respond);								
							$("#qty3").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok3").val(respond);
					}
				})
			}
		});

		$("#qty3").keyup(function(){
			var stok = $("#stok3").val();
			var qty  = $(this).val();
			
			var jml = $("#harga3").val() * $("#qty3").val();
			$("#sub3").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml3").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 3

		// Element 4
		$("#barang_id4").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga4").val(respond);								
							$("#qty4").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok4").val(respond);
					}
				})
			}
		});

		$("#qty4").keyup(function(){
			var stok = $("#stok4").val();
			var qty  = $(this).val();
			
			var jml = $("#harga4").val() * $("#qty4").val();
			$("#sub4").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml4").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 4

		// Element 5
		$("#barang_id5").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga5").val(respond);								
							$("#qty5").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok5").val(respond);
					}
				})
			}
		});

		$("#qty5").keyup(function(){
			var stok = $("#stok5").val();
			var qty  = $(this).val();
			
			var jml = $("#harga5").val() * $("#qty5").val();
			$("#sub5").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml5").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 5
		// Element 6
		$("#barang_id6").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga6").val(respond);								
							$("#qty6").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok6").val(respond);
					}
				})
			}
		});

		$("#qty6").keyup(function(){
			var stok = $("#stok6").val();
			var qty  = $(this).val();
			
			var jml = $("#harga6").val() * $("#qty6").val();
			$("#sub6").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml6").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 6

		// Element 7
		$("#barang_id7").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga7").val(respond);								
							$("#qty7").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok7").val(respond);
					}
				})
			}
		});

		$("#qty7").keyup(function(){
			var stok = $("#stok7").val();
			var qty  = $(this).val();
			
			var jml = $("#harga7").val() * $("#qty7").val();
			$("#sub7").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml7").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 7

		// Element 8
		$("#barang_id8").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga8").val(respond);								
							$("#qty8").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok8").val(respond);
					}
				})
			}
		});

		$("#qty8").keyup(function(){
			var stok = $("#stok8").val();
			var qty  = $(this).val();
			
			var jml = $("#harga8").val() * $("#qty8").val();
			$("#sub8").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml8").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 8

		// Element 9
		$("#barang_id9").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga9").val(respond);								
							$("#qty9").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok9").val(respond);
					}
				})
			}
		});

		$("#qty9").keyup(function(){
			var stok = $("#stok9").val();
			var qty  = $(this).val();
			
			var jml = $("#harga9").val() * $("#qty9").val();
			$("#sub9").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml9").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 9

		// Element 10
		$("#barang_id10").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga10").val(respond);								
							$("#qty10").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok10").val(respond);
					}
				})
			}
		});

		$("#qty10").keyup(function(){
			var stok = $("#stok10").val();
			var qty  = $(this).val();
			
			var jml = $("#harga10").val() * $("#qty10").val();
			$("#sub10").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml10").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 10

		// Element 11
		$("#barang_id11").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga11").val(respond);								
							$("#qty11").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok11").val(respond);
					}
				})
			}
		});

		$("#qty11").keyup(function(){
			var stok = $("#stok11").val();
			var qty  = $(this).val();
			
			var jml = $("#harga11").val() * $("#qty11").val();
			$("#sub11").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml11").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 11

		// Element 12
		$("#barang_id12").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga12").val(respond);								
							$("#qty12").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok12").val(respond);
					}
				})
			}
		});

		$("#qty12").keyup(function(){
			var stok = $("#stok12").val();
			var qty  = $(this).val();
			
			var jml = $("#harga12").val() * $("#qty12").val();
			$("#sub12").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml12").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 12

		// Element 13
		$("#barang_id13").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga13").val(respond);								
							$("#qty13").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok13").val(respond);
					}
				})
			}
		});

		$("#qty13").keyup(function(){
			var stok = $("#stok13").val();
			var qty  = $(this).val();
			
			var jml = $("#harga13").val() * $("#qty13").val();
			$("#sub13").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml13").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 13

		// Element 14
		$("#barang_id14").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga14").val(respond);								
							$("#qty14").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok14").val(respond);
					}
				})
			}
		});

		$("#qty14").keyup(function(){
			var stok = $("#stok14").val();
			var qty  = $(this).val();
			
			var jml = $("#harga14").val() * $("#qty14").val();
			$("#sub14").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml14").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 14

		// Element 15
		$("#barang_id15").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga15").val(respond);								
							$("#qty15").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok15").val(respond);
					}
				})
			}
		});

		$("#qty15").keyup(function(){
			var stok = $("#stok15").val();
			var qty  = $(this).val();
			
			var jml = $("#harga15").val() * $("#qty15").val();
			$("#sub15").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml15").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 15

		// Element 16
		$("#barang_id16").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga16").val(respond);								
							$("#qty16").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok16").val(respond);
					}
				})
			}
		});

		$("#qty16").keyup(function(){
			var stok = $("#stok16").val();
			var qty  = $(this).val();
			
			var jml = $("#harga16").val() * $("#qty16").val();
			$("#sub16").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml16").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 16

		// Element 17
		$("#barang_id17").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga17").val(respond);								
							$("#qty17").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok17").val(respond);
					}
				})
			}
		});

		$("#qty17").keyup(function(){
			var stok = $("#stok17").val();
			var qty  = $(this).val();
			
			var jml = $("#harga17").val() * $("#qty17").val();
			$("#sub17").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml17").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 17

		// Element 18
		$("#barang_id18").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga18").val(respond);								
							$("#qty18").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok18").val(respond);
					}
				})
			}
		});

		$("#qty18").keyup(function(){
			var stok = $("#stok18").val();
			var qty  = $(this).val();
			
			var jml = $("#harga18").val() * $("#qty18").val();
			$("#sub18").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml18").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 18

		// Element 19
		$("#barang_id19").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga19").val(respond);								
							$("#qty19").attr("type","text");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok19").val(respond);
					}
				})
			}
		});

		$("#qty19").keyup(function(){
			var stok = $("#stok19").val();
			var qty  = $(this).val();
			
			var jml = $("#harga19").val() * $("#qty19").val();
			$("#sub19").val(jml);

			var total = 0;
			for (var i = 0; i < 20; i++) {
				if($("#sub"+i).val())
				total = total + parseInt($("#sub"+i).val());
			}			
			$("#total").val(total);

			$.ajax({
				data:{mod:"sub",id:jml},
					success: function(respond){
							$("#jml19").html(respond);
					}
			});
			$.ajax({
				data:{mod:"sub",id:total},
					success: function(res){
							$("#showTotal").html(res);
					}
			});
		});

		// End Element 19




})