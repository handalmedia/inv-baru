<div class="main-content">
	<div class="main-content-inner">
		<!-- #section:basics/content.breadcrumbs -->
		<div class="breadcrumbs" id="breadcrumbs">
			<script type="text/javascript">
				try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
			</script>

			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Admin</a>
				</li>
				<li class="active">Detail Transaksi</li>
			</ul><!-- /.breadcrumb -->

			<!-- /section:basics/content.searchbox -->
		</div>

		<!-- /section:basics/content.breadcrumbs -->
		<div class="page-content">

			<div class="page-header">
				<h1>
					Penjualan
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						Detail Transaksi
					</small>
				</h1>

			</div><!-- /.page-header -->

			<div class="row">
				<button class="btn btn-sm btn-flat btn-success" onclick="window.history.back()" style="margin-left: 12px">Kembali</button>
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<div class="row">
						<div class="col-xs-12">
							<table id="simple-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="center" width="30">No</th>
										<th>Barang</th>
										<th>Qty</th>
										<th>Sub Total</th>
										<th>Ket</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; $jml=0; foreach($detail as $h): ?>
										<tr>
											<td class="center"><?= $no++?></td>
											<td><?= $h->barang_nama ?></td>
											<td><?= $h->tx_detail_qty ?></td>
											<td>Rp. <?= number_format($h->tx_detail_sub) ?></td>	
											<td><?= $h->tx_detail_ket ?></td>	
										</tr>			
										<?php $jml+= $h->tx_detail_sub ;?>							
									<?php endforeach; ?>
									<tr>
										<td></td>
										<td></td>
										<td><b>Total</b></td>
										<td><b>Rp. <?= number_format($jml) ?></b></td>
										<td></td>
									</tr>
								</tbody>
							</table>
							<div>							    
							</div>
						</div><!-- /.span -->
					</div><!-- /.row -->

					<!-- #section:custom/extra.hr -->
					<div class="hr hr32 hr-dotted"></div>
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
