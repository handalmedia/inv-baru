<div class="main-content">
	<div class="main-content-inner">
		<!-- #section:basics/content.breadcrumbs -->
		<div class="breadcrumbs" id="breadcrumbs">
			<script type="text/javascript">
				try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
			</script>

			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Admin</a>
				</li>
				<li class="active">Transaksi Penjualan</li>
			</ul><!-- /.breadcrumb -->

			<!-- /section:basics/content.searchbox -->
		</div>

		<!-- /section:basics/content.breadcrumbs -->
		<div class="page-content">

			<div class="page-header">
				<h1>
					Penjualan
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						List Penjualan
					</small>
				</h1>
				<div class="pull-right">
					<a href="<?=base_url('penjualan/txbaru')?>" role="button" class="green"> 
						<button class="btn btn-success">Transaksi Baru</button>
					</a>
				</div>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<?php if($this->session->flashdata('berhasil')): ?>
						<div class="alert alert-block alert-success">
							<button type="button" class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
							<?= $this->session->flashdata('berhasil') ?>
						</div>
					<?php endif; ?>

					<?php if($this->session->flashdata('gagal')): ?>
						<div class="alert alert-block alert-danger">
							<button type="button" class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
							<?= $this->session->flashdata('gagal') ?>
						</div>
					<?php endif; ?>

					<div class="row">
						<div class="col-xs-12">
							<table id="simple-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="center" width="30">No</th>
										<th>Id Transaksi</th>
										<th>Tanggal</th>
										<th>Total</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; foreach($tx as $h): ?>
										<tr>
											<td class="center"><?= $no++?></td>

											<td><?= $h->tx_id ?></td>
											<td><?= $h->tx_tgl ?></td>
											<td>Rp. <?= number_format($h->tx_total) ?></td>
											<td>
												<div>
													<a href="<?=base_url('penjualan/detail/'.$h->tx_id)?>">
														<button class="btn btn-xs btn-success">
															<i class="ace-icon fa fa-list bigger-120"></i>
														</button>
													</a>
												</div>
												<div id="modal-edit<?=$h->tx_id?>" class="modal fade" tabindex="-1">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<div class="table-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		<span class="white">&times;</span>
																	</button>
																	Detail Transaksi
																</div>
															</div>
																<div class="modal-body">
																	<table class="table table-hover">
																		<thead>
																			<th>Barang</th>
																			<th>Qty</th>
																			<th>Subtotal</th>
																		</thead>
																		<tbody id="isi">
																			
																		</tbody>
																	</table>
																</div>
																<div class="modal-footer no-margin-top">
																	<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
																		<i class="ace-icon fa fa-times"></i>
																		Tutup
																	</button>
																</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>

											</td>
										</tr>
										<input type="hidden" id="tx_id<?=$no?>" value="<?=$h->tx_id?>">
									<?php endforeach; ?>
									<input type="hidden" id="max" value="<?=$no?>">
								</tbody>
							</table>
							<div>
								
									<?php 
							          echo $this->pagination->create_links();
							        ?>
							    
							</div>
						</div><!-- /.span -->
					</div><!-- /.row -->

					<!-- #section:custom/extra.hr -->
					<div class="hr hr32 hr-dotted"></div>


					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
