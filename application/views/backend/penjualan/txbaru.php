<div class="main-content">
	<div class="main-content-inner">
		<!-- #section:basics/content.breadcrumbs -->
		<div class="breadcrumbs" id="breadcrumbs">
			<script type="text/javascript">
				try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
			</script>

			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Admin</a>
				</li>
				<li class="active">Transaksi Penjualan</li>
			</ul><!-- /.breadcrumb -->

			<!-- /section:basics/content.searchbox -->
		</div>

		<!-- /section:basics/content.breadcrumbs -->
		<div class="page-content">

			<div class="page-header" style="margin-bottom: 30px">
				<h1>
					Penjualan
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						Transaksi Baru
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<input type="hidden" id="base" value="<?=base_url()?>">
					<!-- PAGE CONTENT BEGINS -->
					<?php if($this->session->flashdata('berhasil')): ?>
						<div class="alert alert-block alert-success">
							<button type="button" class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
							<?= $this->session->flashdata('berhasil') ?>
						</div>
					<?php endif; ?>

					<?php if($this->session->flashdata('gagal')): ?>
						<div class="alert alert-block alert-danger">
							<button type="button" class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
							<?= $this->session->flashdata('gagal') ?>
						</div>
					<?php endif; ?>
					<form method="POST" action="<?=base_url('penjualan/simpan')?>">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-4">
								<div class="form-group">
									<label class="control-label" for="txid">Kode Transaksi</label>
									<div class="clearfix">
										<input type="text" name="txid" id="txid" class="form-control" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label" for="txid">Tanggal</label>
									<div class="clearfix">
										<input type="text" name="txtgl" id="txtgl" class="form-control" readonly value="<?= date("Y-m-d")?>" >
									</div>
								</div>
							</div>
							<div class="col-xs-5">
									<div class="form-group">
									<label class="control-label" for="total">Total</label>
									<div class="clearfix">
										<div style="font-size: 60px;" id="showTotal">Rp. 0</div>
										<input type="hidden" name="total" id="total" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-xs-3">
								<button  onclick="return confirm('Simpan Data Transaki?')" style="height: 100px" type="submit" class="btn btn-lg btn-success btn-flat pull-right"><span class="fa fa-save"></span> Simpan</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<b>*Centang Bagian Mn Untuk Harga Manual</b>
							<table id="simple-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="center" width="30">No</th>
										<th>Barang</th>
										<th>Mn</th>
										<th>Harga</th>
										<th>Stok</th>
										<th>Qty</th>
										<th>Sub Total</th>
									</tr>
								</thead>
								<tbody>
									<?php 	for ($i=0; $i < 20; $i++): ?>
										<tr>
											<td class="center"><?= $i+1?></td>
											<td width="250">
												<select name="barang_id<?=$i?>" class="form-control" id="barang_id<?=$i?>">
														<option value="0">--Pilih Barang--</option>
													<?php foreach($barang as $b): ?>
														<option value="<?=$b->barang_id?>"><?=$b->barang_nama?></option>
													<?php endforeach; ?>
												</select>
											</td>
											<td width="40"><input type="checkbox" name="cek<?=$i?>" id="cek<?=$i?>" value="0"></td>
											<td width="200"><input class="form-control" type="number" name="harga<?=$i?>" id="harga<?=$i?>" readonly></td>
											<td width="150"><input class="form-control" type="number" name="stok<?=$i?>" id="stok<?=$i?>" readonly></td>
											<td width="150"><input class="form-control" type="hidden" name="qty<?=$i?>" id="qty<?=$i?>"></td>
											<td width="150"><div id="jml<?=$i?>"></div>
												<input class="form-control" type="hidden" name="sub<?=$i?>" id="sub<?=$i?>">
											</td>											
										</tr>
									<?php endfor; ?>
								</tbody>
							</table>
							<div>
							    
							</div>
						</div><!-- /.span -->
					</div><!-- /.row -->
					</form>
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<script type="text/javascript" src="<?=base_url('assets/js/')?>tx.js"></script>
<script type="text/javascript">
	$( document ).ready(function() {
		var b = $("#base").val();
	    $.ajax({
	    		type:"POST",
				url: b+"/penjualan/get_last",
				data:{id:1},
				success: function(respond){
						$("#txid").val(respond);			
				}
		});

		$("#cek0").change(function(){
			var cek = $("#cek0").val();
			if(cek == 0){
				$("#harga0").attr('readonly',false);
				$("#cek0").val(1);
			}else{
				$("#harga0").attr('readonly',true);
				$("#cek0").val(0);
				$("#barang_id0").prop('selectedIndex', 0);
			}
		});

		$("#cek1").change(function(){
			var cek = $("#cek1").val();
			if(cek == 0){
				$("#harga1").attr('readonly',false);
				$("#cek1").val(1);
			}else{
				$("#harga1").attr('readonly',true);
				$("#cek1").val(0);
				$("#barang_id1").prop('selectedIndex', 0);
			}
		});
		$("#cek2").change(function(){
			var cek = $("#cek2").val();
			if(cek == 0){
				$("#harga2").attr('readonly',false);
				$("#cek2").val(1);
			}else{
				$("#harga2").attr('readonly',true);
				$("#cek2").val(0);
				$("#barang_id2").prop('selectedIndex', 0);
			}
		});
		$("#cek3").change(function(){
			var cek = $("#cek3").val();
			if(cek == 0){
				$("#harga3").attr('readonly',false);
				$("#cek3").val(1);
			}else{
				$("#harga3").attr('readonly',true);
				$("#cek3").val(0);
				$("#barang_id3").prop('selectedIndex', 0);
			}
		});
		$("#cek4").change(function(){
			var cek = $("#cek4").val();
			if(cek == 0){
				$("#harga4").attr('readonly',false);
				$("#cek4").val(1);
			}else{
				$("#harga4").attr('readonly',true);
				$("#cek4").val(0);
				$("#barang_id4").prop('selectedIndex', 0);
			}
		});

		$("#cek5").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga5").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga5").attr('readonly',true);
				$(this).val(0);
				$("#barang_id5").prop('selectedIndex', 0);
			}
		});

		$("#cek6").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga6").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga6").attr('readonly',true);
				$(this).val(0);
				$("#barang_id6").prop('selectedIndex', 0);
			}
		});

		$("#cek7").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga7").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga7").attr('readonly',true);
				$(this).val(0);
				$("#barang_id7").prop('selectedIndex', 0);
			}
		});

		$("#cek8").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga8").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga8").attr('readonly',true);
				$(this).val(0);
				$("#barang_id8").prop('selectedIndex', 0);
			}
		});


		$("#cek9").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga9").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga9").attr('readonly',true);
				$(this).val(0);
				$("#barang_id9").prop('selectedIndex', 0);
			}
		});

		$("#cek10").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga10").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga10").attr('readonly',true);
				$(this).val(0);
				$("#barang_id10").prop('selectedIndex', 0);
			}
		});

		$("#cek11").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga11").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga11").attr('readonly',true);
				$(this).val(0);
				$("#barang_id11").prop('selectedIndex', 0);
			}
		});

		$("#cek12").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga12").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga12").attr('readonly',true);
				$(this).val(0);
				$("#barang_id12").prop('selectedIndex', 0);
			}
		});

		$("#cek13").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga13").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga13").attr('readonly',true);
				$(this).val(0);
				$("#barang_id13").prop('selectedIndex', 0);
			}
		});

		$("#cek14").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga14").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga14").attr('readonly',true);
				$(this).val(0);
				$("#barang_id14").prop('selectedIndex', 0);
			}
		});

		$("#cek15").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga15").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga15").attr('readonly',true);
				$(this).val(0);
				$("#barang_id15").prop('selectedIndex', 0);
			}
		});

		$("#cek16").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga16").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga16").attr('readonly',true);
				$(this).val(0);
				$("#barang_id16").prop('selectedIndex', 0);
			}
		});

		$("#cek17").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga17").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga17").attr('readonly',true);
				$(this).val(0);
				$("#barang_id17").prop('selectedIndex', 0);
			}
		});

		$("#cek18").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga18").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga18").attr('readonly',true);
				$(this).val(0);
				$("#barang_id18").prop('selectedIndex', 0);
			}
		});

		$("#cek19").change(function(){
			var cek = $(this).val();
			if(cek == 0){
				$("#harga19").attr('readonly',false);
				$(this).val(1);
			}else{
				$("#harga19").attr('readonly',true);
				$(this).val(0);
				$("#barang_id19").prop('selectedIndex', 0);
			}
		});

	});
</script>