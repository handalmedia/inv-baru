$(function(){

	$.ajaxSetup({
		type:"POST",
		url: "<?= base_url('penjualan/ambil_data') ?>",
		cache: false,
	});
		// Element 0
		$("#barang_id0").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
				data:{mod:"harga",id:value},
					success: function(respond){
							$("#harga0").val(respond);								
							$("#qty0").attr("type","number");
					}
				});
				$.ajax({
				data:{mod:"stok",id:value},
					success: function(respond){
							$("#stok0").val(respond);
							$("#qty0").attr("max",respond);
							$("#qty0").attr("min",1);
					}
				})
			}
		});

		$("#qty0").keyup(function(){
			var stok = $("#stok0").val();
			var qty  = $(this).val();
			if (qty > stok || qty < 1) {
				$("#qty0").val(stok);
			}
		});
	

})