  <?php
 
  header("Content-Type: application/force-download");
  header("Cache-Control: no-cache, must-revalidate"); 
  header("Expires: Sat, 26 Jul 2050 05:00:00 GMT"); 
  header("content-disposition: attachment;filename=laporan_penjualan_".date('d-m-Y').".xls");
 
 ?>
 <table border="1" style="border-collapse: collapse;">
 	<thead>
 		<tr>
 			<th colspan="5"><b>LAPORAN PENJUALAN TOKO</b></th>
 		</tr>
 		<tr>
 			<th>No</th>
 			<th width="400">Barang</th>
 			<th width="200">Total (pcs)</th>
 			<th width="200">Harga Total</th>
 			<th width="200">Tanggal</th>
 		</tr>
 	</thead>
 	<tbody>
 		<?php if($bulan != NULL && $tahun == NULL): ?>
	 		<?php $n=1; foreach($data as $d): ?>
	 			<tr>
	 				<td><?=$n++?></td>
	 				<td><?=$d->barang_nama?></td>
	 				<td><?=$d->Qty?></td>
	 				<td>Rp. <?=number_format($d->Total)?></td>
	 				<td><?=$d->tx_tgl?></td>
	 			</tr>
	 		<?php endforeach ?>
 		<?php elseif($tahun != NULL): ?>
 			<!-- Tahunan -->
 			
 			<tr><td colspan="5"><center><b>Bulan <?=$listbulan[0]?></b></center></td></tr>
	 			<?php $n=1; foreach($data1 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[1]?></b></center></td></tr>
	 			<?php $n=1; foreach($data2 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[2]?></b></center></td></tr>
	 			<?php $n=1; foreach($data3 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[3]?></b></center></td></tr>
	 			<?php $n=1; foreach($data4 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[4]?></b></center></td></tr>
	 			<?php $n=1; foreach($data5 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[5]?></b></center></td></tr>
	 			<?php $n=1; foreach($data6 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[6]?></b></center></td></tr>
	 			<?php $n=1; foreach($data7 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[7]?></b></center></td></tr>
	 			<?php $n=1; foreach($data8 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[8]?></b></center></td></tr>
	 			<?php $n=1; foreach($data9 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[9]?></b></center></td></tr>
	 			<?php $n=1; foreach($data10 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[10]?></b></center></td></tr>
	 			<?php $n=1; foreach($data11 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[11]?></b></center></td></tr>
	 			<?php $n=1; foreach($data12 as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>
		 	
	 		<!-- End Tahunan -->
 		<?php else: ?>
 			<?php foreach($allth as $th): ?>
 				<tr><td colspan="5"><center><b>Bulan <?=$listbulan[0]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data1[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[1]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data2[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[2]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data3[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[3]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data4[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[4]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data5[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[5]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data6[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[6]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data7[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[7]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data8[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[8]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data9[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>
		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[9]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data10[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[10]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data11[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>

		 	<tr><td colspan="5"><center><b>Bulan <?=$listbulan[11]?> <?= $th->Th ?></b></center></td></tr>
	 			<?php $n=1; foreach($data12[$th->Th] as $d): ?>			 		
			 			<tr>
			 				<td><?=$n++?></td>
			 				<td><?=$d->barang_nama?></td>
			 				<td><?=$d->Qty?></td>
			 				<td>Rp. <?=number_format($d->Total)?></td>
	 						<td><?=$d->tx_tgl?></td>
			 			</tr>			 		
		 		<?php endforeach ?>
		 	<tr><td colspan="5"><center>----------------------------------</center></td></tr>
 			<?php endforeach;?>
 		<?php endif ?>
 	</tbody>
 </table>