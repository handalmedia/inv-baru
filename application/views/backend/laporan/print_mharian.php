  <?php
 
  header("Content-Type: application/force-download");
  header("Cache-Control: no-cache, must-revalidate"); 
  header("Expires: Sat, 26 Jul 2050 05:00:00 GMT"); 
  header("content-disposition: attachment;filename=stok_masuk_harian_".date('d-m-Y').".xls");
 
 ?>
 <table border="1" style="border-collapse: collapse;">
 	<thead>
 		<tr>
 			<th colspan="3"><b>LAPORAN STOK BARANG MASUK</b></th>
 		</tr>
 		<tr>
 			<th>No</th>
 			<th width="400">Barang</th>
 			<th width="200">Total (pcs)</th>
 		</tr>
 	</thead>
 	<tbody>
 		<?php $n=1; foreach($list as $d): ?>
 			<tr>
 				<td><?=$n++?></td>
 				<td><?=$d->barang_nama?></td>
 				<td><?=$d->Qty?></td>
 			</tr>
	 	<?php endforeach ?>
	 </tbody>
 </table>