  <?php
 
  header("Content-Type: application/force-download");
  header("Cache-Control: no-cache, must-revalidate"); 
  header("Expires: Sat, 26 Jul 2050 05:00:00 GMT"); 
  header("content-disposition: attachment;filename=laporan_penjualan_harian_".date('d-m-Y').".xls");
 
 ?>
 <table border="1" style="border-collapse: collapse;">
 	<thead>
 		<tr>
 			<th colspan="5"><b>LAPORAN PENJUALAN TOKO</b></th>
 		</tr>
 		<tr>
 			<th>No</th>
 			<th width="400">Barang</th>
 			<th width="200">Total (pcs)</th>
 			<th width="200">Harga Total</th>
 			<th width="200">Tanggal</th>
 		</tr>
 	</thead>
 	<tbody>
 		
	 		<?php $n=1; foreach($list as $d): ?>
	 			<tr>
	 				<td><?=$n++?></td>
	 				<td><?=$d->barang_nama?></td>
	 				<td><?=$d->Qty?></td>
	 				<td>Rp. <?=number_format($d->Total)?></td>
	 				<td><?=$d->tx_tgl?></td>
	 			</tr>
	 		<?php endforeach ?>

 	</tbody>
 </table>