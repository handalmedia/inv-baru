<div class="main-content">
	<div class="main-content-inner">
		<!-- #section:basics/content.breadcrumbs -->
		<div class="breadcrumbs" id="breadcrumbs">
			<script type="text/javascript">
				try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
			</script>

			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Dashboard</li>
			</ul><!-- /.breadcrumb -->

			<!-- #section:basics/content.searchbox -->
			<div class="nav-search" id="nav-search">
				<form class="form-search">
					<span class="input-icon">
						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
						<i class="ace-icon fa fa-search nav-search-icon"></i>
					</span>
				</form>
			</div><!-- /.nav-search -->

			<!-- /section:basics/content.searchbox -->
		</div>

		<!-- /section:basics/content.breadcrumbs -->
		<div class="page-content">
			
			<div class="page-header">
				<h1>
					Dashboard
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						overview &amp; stats
					</small>
				</h1>
			</div><!-- /.page-header -->
			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<div class="row">
						<div class="col-md-12 infobox-container">
							<!-- #section:pages/dashboard.infobox -->
							<div class="infobox infobox-green">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-gift"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number"><?= $jml_barang?></span>
									<div class="infobox-content">Jenis Barang</div>
								</div>
								<!-- /section:pages/dashboard.infobox.stat -->
							</div>

							<div class="infobox infobox-blue">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-shopping-cart"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number"><?= $jml_tx?></span>
									<div class="infobox-content">Transaksi</div>
								</div>
							</div>

							<div class="infobox infobox-pink">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-bookmark"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number"><?= $menipis?></span>
									<div class="infobox-content">Stok Menipis</div>
								</div>
							</div>

							<div class="infobox infobox-red" style="width: 300px">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-credit-card"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number">Rp. <?= number_format($total)?></span>
									<div class="infobox-content">Penjualan Bulan Ini</div>
								</div>
							</div>

							<!-- /section:pages/dashboard.infobox -->
						</div>
					</div>

					<div class="row">
						<div class="space-8"></div>
						<div class="col-sm-6">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter">
										<i class="ace-icon fa fa-star orange"></i>
										Barang Favorit
									</h4>

									<div class="widget-toolbar">
										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</div>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped">
											<thead class="thin-border-bottom">
												<tr>
													<th>
														No
													</th>
													<th>
														<i class="ace-icon fa fa-caret-right blue"></i>Barang
													</th>
													<th>
														<i class="ace-icon fa fa-caret-right blue"></i>Stok
													</th>
													<th>
														<i class="ace-icon fa fa-caret-right blue"></i>Total Harga
													</th>
													<th class="hidden-480">
														<i class="ace-icon fa fa-caret-right blue"></i>Total Jual (pcs)
													</th>
												</tr>
											</thead>

											<tbody>
												<?php $no=1 ;foreach($favorit as $f): ?>
													<tr>
														<td><?=$no++?></td>
														<td><?=$f->barang_nama?></td>
														<td><?=$f->barang_stok?></td>
														<td>Rp. <?= number_format($f->Total)?></td>
														<td><?=$f->Qty?></td>																	
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div><!-- /.col -->
						
						<div class="vspace-12-sm"></div>
						<div class="col-sm-6">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter">
										<i class="ace-icon fa fa-star red"></i>
										Barang Stok Menipis
									</h4>

									<div class="widget-toolbar">
										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</div>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped">
											<thead class="thin-border-bottom">
												<tr>
													<th>
														No
													</th>
													<th>
														<i class="ace-icon fa fa-caret-right blue"></i>Barang
													</th>
													<th>
														<i class="ace-icon fa fa-caret-right blue"></i>Stok
													</th>
													<th>
														<i class="ace-icon fa fa-caret-right blue"></i>Harga
													</th>
												</tr>
											</thead>

											<tbody>
												<?php $no=1 ;foreach($listmenipis as $f): ?>
													<tr>
														<td><?=$no++?></td>
														<td><?=$f->barang_nama?></td>
														<td><?=$f->barang_stok?></td>
														<td>Rp. <?= number_format($f->barang_harga)?></td>												
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div><!-- /.col -->

						
					</div><!-- /.row -->
					<div class="hr hr32 hr-dotted"></div>
					<div class="row">
						<div class="col-sm-8">
							<div class="widget-box transparent" id="recent-box">
								<div class="widget-header">
									<h4 class="widget-title lighter smaller">
										<i class="ace-icon fa fa-rss orange"></i>LAPORAN HARI INI
									</h4>

									<div class="widget-toolbar no-border">
										<ul class="nav nav-tabs" id="recent-tab">
											<li class="active">
												<a data-toggle="tab" href="#hmasuk-tab">Stok Masuk</a>
											</li>
											<li>
												<a data-toggle="tab" href="#hpenjualan-tab">Penjualan</a>
											</li>
										</ul>
									</div>
								</div>

								<div class="widget-body">
									<div class="widget-main padding-4">
										<div class="tab-content padding-8">
											<div id="hmasuk-tab" class="tab-pane active">
												<h4 class="smaller lighter green">
													<i class="ace-icon fa fa-list"></i>
													Laporan Stok Masuk 
												</h4>
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Barang</th>
															<th>Total (pcs)</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($mharian as $m): ?>
															<tr>
																<td><?=$m->barang_nama?></td>
																<td><?=$m->Qty?></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
												<center>
													<a href="<?=base_url('dashboard/print_mharian')?>">
														<button class="btn btn-flat btn-info">Cetak</button>
													</a>
												</center>

												<!-- /section:pages/dashboard.tasks -->
											</div>

											<div id="hpenjualan-tab" class="tab-pane">
												<!-- #section:pages/dashboard.comments -->
												<h4 class="smaller lighter green">
													<i class="ace-icon fa fa-list"></i>
													Laporan Penjualan 
												</h4>
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Barang</th>
															<th>Qty (pcs)</th>
															<th>Total (pcs)</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($jharian as $m): ?>
															<tr>
																<td><?=$m->barang_nama?></td>
																<td><?=$m->Qty?></td>
																<td>Rp. <?=number_format($m->Total)?></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
												<center>
													<a href="<?=base_url('dashboard/print_mharian')?>">
														<button class="btn btn-flat btn-info">Cetak</button>
													</a>
												</center>
												<!-- /section:pages/dashboard.comments -->
											</div>
										</div>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div><!-- /.col -->

					</div><!-- /.row -->
					<div class="hr hr32 hr-dotted"></div>

					<div class="row">
						<div class="col-sm-8">
							<div class="widget-box transparent" id="recent-box">
								<div class="widget-header">
									<h4 class="widget-title lighter smaller">
										<i class="ace-icon fa fa-rss orange"></i>LAPORAN BULAN INI
									</h4>

									<div class="widget-toolbar no-border">
										<ul class="nav nav-tabs" id="recent-tab">
											<li class="active">
												<a data-toggle="tab" href="#masuk-tab">Stok Masuk</a>
											</li>

											<li class="hidden">
												<a data-toggle="tab" href="#keluar-tab">Stok Keluar</a>
											</li>

											<li>
												<a data-toggle="tab" href="#penjualan-tab">Penjualan</a>
											</li>
										</ul>
									</div>
								</div>

								<div class="widget-body">
									<div class="widget-main padding-4">
										<div class="tab-content padding-8">
											<div id="masuk-tab" class="tab-pane active">
												<h4 class="smaller lighter green">
													<i class="ace-icon fa fa-list"></i>
													Laporan Stok Masuk 
												</h4>
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Barang</th>
															<th>Total (pcs)</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($masuk as $m): ?>
															<tr>
																<td><?=$m->barang_nama?></td>
																<td><?=$m->Qty?></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
												<center>
													<a href="#modal-masuk" role="button" class="green" data-toggle="modal">
														<button class="btn btn-flat btn-info"><i class="fa fa-print"></i> Selengkapnya</button>
													</a>
												</center>
												<div id="modal-masuk" class="modal fade" tabindex="-1">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<div class="table-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		<span class="white">&times;</span>
																	</button>
																	Laporan Stok Masuk
																</div>
															</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_masuk/'.date('m').'/0')?>">
																				<button class="btn btn-success">Bulan Ini</button></a>
																			</center>
																		</div>
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_masuk/0/'.date('Y'))?>">
																				<button class="btn btn-info">Tahun Ini</button></a>
																			</center>
																		</div>
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_masuk/0/0')?>">
																				<button class="btn btn-warning"> Semua </button></a></center>
																		</div>
																	</div>
																</div>
															</form>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>

												<!-- /section:pages/dashboard.tasks -->
											</div>

											<div id="keluar-tab" class="tab-pane hidden">
												<h4 class="smaller lighter green">
													<i class="ace-icon fa fa-list"></i>
													Laporan Stok Keluar 
												</h4>
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Barang</th>
															<th>Total (pcs)</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($keluar as $m): ?>
															<tr>
																<td><?=$m->barang_nama?></td>
																<td><?=$m->Qty?></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
												<center>
													<a href="#modal-keluar" role="button" class="green" data-toggle="modal">
														<button class="btn btn-flat btn-info"><i class="fa fa-print"></i> Selengkapnya</button>
													</a>
												</center>
												<div id="modal-keluar" class="modal fade" tabindex="-1">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<div class="table-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		<span class="white">&times;</span>
																	</button>
																	Laporan Stok Keluar
																</div>
															</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_keluar/'.date('m').'/0')?>">
																				<button class="btn btn-success">Bulan Ini</button></a>
																			</center>
																		</div>
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_keluar/0/'.date('Y'))?>">
																				<button class="btn btn-info">Tahun Ini</button></a>
																			</center>
																		</div>
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_keluar/0/0')?>">
																				<button class="btn btn-warning"> Semua </button></a></center>
																		</div>
																	</div>
																</div>
															</form>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
											</div><!-- /.#member-tab -->

											<div id="penjualan-tab" class="tab-pane">
												<!-- #section:pages/dashboard.comments -->
												<h4 class="smaller lighter green">
													<i class="ace-icon fa fa-list"></i>
													Laporan Penjualan 
												</h4>
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Barang</th>
															<th>Qty (pcs)</th>
															<th>Total (pcs)</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($jual as $m): ?>
															<tr>
																<td><?=$m->barang_nama?></td>
																<td><?=$m->Qty?></td>
																<td>Rp. <?=number_format($m->Total)?></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
												<center>
													<a href="#modal-penjualan" role="button" class="green" data-toggle="modal">
														<button class="btn btn-flat btn-info"><i class="fa fa-print"></i> Selengkapnya</button>
													</a>
												</center>
												<div id="modal-penjualan" class="modal fade" tabindex="-1">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<div class="table-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		<span class="white">&times;</span>
																	</button>
																	Laporan Penjualan
																</div>
															</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_penjualan/'.date('m').'/0')?>">
																				<button class="btn btn-success">Bulan Ini</button></a>
																			</center>
																		</div>
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_penjualan/0/'.date('Y'))?>">
																				<button class="btn btn-info">Tahun Ini</button></a>
																			</center>
																		</div>
																		<div class="col-sm-4">
																			<center><a href="<?=base_url('dashboard/print_penjualan/0/0')?>">
																				<button class="btn btn-warning"> Semua </button></a></center>
																		</div>
																	</div>
																</div>
															</form>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
												<!-- /section:pages/dashboard.comments -->
											</div>
										</div>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div><!-- /.col -->
						<div class="col-sm-4">
							<center>
								<a href="<?=base_url('penjualan/txbaru')?>">
									<button style="height: 100px" class="btn btn-warning btn-flat">
										<div class="infobox infobox-red" style="background-color: transparent;">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-credit-card"></i>
											</div>
											<div class="infobox-data">
												<span class="infobox-data-number">+ Penjualan</span>
											</div>
										</div>
									</button>
								</a>
							</center>
						</div>
					</div><!-- /.row -->

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->