<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->auth->restrict();
        $this->load->library('session');
        $this->load->model('mdl_barang');
        $this->load->model('mdl_gudang');
        $this->load->model('mdl_inventory');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
        $data['gudang'] = $this->mdl_gudang->get_data();
		$this->load->view('backend/header');
		$this->load->view('backend/gudang/mutasi',$data);
		$this->load->view('backend/footer');
	}

	public function proses()
	{


        $gudang_id = $this->input->post('gudang_id');
        $gudang_id2 = $this->input->post('gudang_id2');
        $barang_id = $this->input->post('barang_id');
        $stok = $this->input->post('stok');
        $qty = $this->input->post('qty');
        if($qty > $stok){
            $this->session->set_flashdata('gagal','Qty Melebihi Stok Asal');
            redirect('mutasi','refresh');
        }else{

            $gudangawal = $this->mdl_gudang->get_row_barang($barang_id,$gudang_id);
            $dtawal = array(
                        'gudang_stok' => $gudangawal->gudang_stok - $qty
                    );
            $whawal = array(
                        'gudang_id' => $gudang_id,
                        'barang_id' => $barang_id,
                    ); 
            $this->mdl_gudang->update_data($whawal,$dtawal,'tbl_gudang_detail');
            //-----------
            $cekgudang = $this->mdl_gudang->get_row_barang($barang_id,$gudang_id2);

            if ($cekgudang == NULL) {
                $data = array(
                        'gudang_id' => $gudang_id2,
                        'barang_id' => $barang_id,
                        'gudang_stok' => $qty
                    );
                $this->mdl_gudang->input_data($data,'tbl_gudang_detail');
            }else{
                $dttujuan = array( 'gudang_stok' => $cekgudang->gudang_stok + $qty );
                $whtujuan = array(
                                'gudang_id' => $gudang_id2,
                                'barang_id' => $barang_id,
                            ); 
                $this->mdl_gudang->update_data($whtujuan,$dttujuan,'tbl_gudang_detail');
            }

            $data2 = array(
                        'jenis_id' => '4',
                        'barang_id' => $barang_id,
                        'inv_qty' => $qty,
                        'keterangan' => 'Mutasi Gudang'
                    );        
            $this->mdl_inventory->input_data($data2,'tbl_inventory');            
            
            $this->session->set_flashdata('berhasil','Data Berhasil Di Mutasi');
            redirect('gudang','refresh');   
        }
		
	}

    function ambil_data(){

        $id=$this->input->post('id');
        $mod=$this->input->post('mod');
        if ($mod == 'gudang') {
            $data = $this->mdl_gudang->get_data_barang($id);
            $opt = '<option value="0">--Pilih Barang--</option>';
            foreach ($data as $d) {
                $opt .= '<option value="'.$d->barang_id.'">'.$d->barang_nama.'</option>';
            }

            echo $opt;

        }elseif($mod=='tujuan'){
            
            $data = $this->mdl_gudang->get_gudang_ex($id);
            $opt = '<option value="0">--Pilih Gudang--</option>';
            foreach ($data as $d) {
                $opt .= '<option value="'.$d->gudang_id.'">'.$d->gudang_nama.'</option>';
            }

            echo $opt;

        }elseif($mod == 'barang'){
            $idg=$this->input->post('idg');
            $data = $this->mdl_gudang->get_row_barang($id,$idg);
            echo $data->gudang_stok;
        }
        
        
    }

}
