<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->auth->restrict();
        $this->load->library('session');
        $this->load->model('mdl_tx');
        $this->load->model('mdl_barang');
        $this->load->model('mdl_tx_detail');
        $this->load->model('mdl_inventory');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$jumlah_data = $this->mdl_tx->jml_data();
        $this->load->library('pagination');
        $config['base_url'] = base_url('barang/index');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 20;
        $from = $this->uri->segment(4);
        $this->pagination->initialize($config);
        $data['tx'] = $this->mdl_tx->data($config['per_page'],$from);

		//$data['hotel'] = $this->mdl_masterhotel->get_data();
		$this->load->view('backend/header');
		$this->load->view('backend/penjualan/index',$data);
		$this->load->view('backend/footer');
	}

    public function txbaru()
    {
        $data['barang'] = $this->mdl_barang->get_data();

        $this->load->view('backend/header');
        $this->load->view('backend/penjualan/txbaru',$data);
        $this->load->view('backend/footer');
    }

	public function simpan()
	{
        // Modul Tabel Transaksi
        $txid = $this->input->post('txid');
        $txtgl = $this->input->post('txtgl');
        $total = $this->input->post('total');

        $datatx = array(
            'tx_id' => $txid,
            'tx_total' => $total
            );
        $this->mdl_tx->input_data($datatx,'tbl_tx');

        
        for ($i=0; $i < 5; $i++) { 

            $barang_id = $this->input->post('barang_id'.$i);
            $harga = $this->input->post('harga'.$i);
            $qty = $this->input->post('qty'.$i);
            $sub = $this->input->post('sub'.$i);
            $cek = $this->input->post('cek'.$i);
            $ket ='';
            if($cek == 1){
                $ket = 'Harga Potongan Rp. '.number_format($harga);
            }else{
                $ket = 'Harga Normal';
            }
            //data detail
            $datadt = array(
                'tx_id' => $txid,
                'barang_id' => $barang_id,
                'tx_detail_qty' => $qty,
                'tx_detail_sub' => $sub,
                'tx_detail_ket' => $ket
                );

            $stok = $this->mdl_barang->get_stok($barang_id);
            //data update stok barang
            $databr = array(
                'barang_stok' => $stok - $qty
                );
            $where = array(
                'barang_id' => $barang_id
            );            

            //data inventori jual
            $datainv = array(
                'jenis_id' => 1,
                'barang_id' => $barang_id,
                'inv_qty' => $qty
                );

            if($barang_id != 0){
                //input Detail Transaksi
                $this->mdl_tx_detail->input_data($datadt,'tbl_tx_detail');
                //Update stok barang
                $this->mdl_barang->update_data($where,$databr,'tbl_barang');
                //Input data inventory
                $this->mdl_inventory->input_data($datainv,'tbl_inventory');

            }            
        }    

        $this->session->set_flashdata('berhasil','Transaksi Berhasil');
        redirect('penjualan','refresh');     

		
	}

	public function ubah(){
		$id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $harga = $this->input->post('harga');

        $data = array(
            'barang_nama' => $nama,
            'barang_harga' => $harga
            );
        $where = array(
            'barang_id' => $id
        );
        $this->mdl_tx->update_data($where,$data,'tbl_barang');
        $this->session->set_flashdata('berhasil','Data Berhasil Di Ubah');
        redirect('barang','refresh'); 
	}

    public function hapus($id){
        $where = array(
            'barang_id' => $id
        );
        $del =$this->mdl_tx->hapus_data($where,'tbl_barang');

        $this->session->set_flashdata('berhasil','Data Berhasil Di Hapus');

        redirect('penjualan','refresh'); 

    }

    function ambil_data(){

        $id=$this->input->post('id');
        $mod=$this->input->post('mod');
        $data = $this->mdl_barang->get_row($id);      
        if($mod=="harga"){
            echo $data->barang_harga;
        }elseif ($mod=="stok") {
            echo $data->barang_stok;
        }elseif ($mod=="sub") {
            $jml = number_format($id);
            echo "Rp. ".$jml;

        }
    }

    function detail($txid){

        $data['detail'] = $this->mdl_tx_detail->get_detail($txid);

        $this->load->view('backend/header');
        $this->load->view('backend/penjualan/detail_tx',$data);
        $this->load->view('backend/footer');
    }

    function get_last(){
        $id=$this->input->post('id');

        $data = $this->mdl_tx->get_last();
        if(!empty($data)):
            $lastkey =  $data->tx_id;
            $getMonth = substr($lastkey, 4,4);
            if($getMonth == date('md')){
                $explode = substr($lastkey, 8,3);
                $ConvToInt = intval($explode);
                $size = strlen($ConvToInt);
                $new = $ConvToInt+1;
                $key='';
                if($size == 1){
                    $key = '00'.$new;
                }elseif($size == 2){
                    $key = '0'.$new;
                }else{
                    $key = $new;
                }
            }else{
                $key = '001';
            }
        else:
            $key = '001';
        endif;

        echo date('Ymd').$key;
        //echo $getMonth;
    }

}
