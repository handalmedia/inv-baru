<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stokgudang extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->auth->restrict();
        $this->load->library('session');
        //$this->load->model('mdl_stok');
        $this->load->model('mdl_barang');
        $this->load->model('mdl_gudang');
        $this->load->model('mdl_inventory');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$data['gudang'] = $this->mdl_gudang->get_data();
		$this->load->view('backend/header');
		$this->load->view('backend/gudang/stokgudang',$data);
		$this->load->view('backend/footer');
	}


	public function instok(){
        $gudang_id = $this->input->post('gudang_id');
		$barang_id = $this->input->post('barang_id');
        $stok = $this->input->post('stok');
        $qty = $this->input->post('qty');

        $data = array(
            'gudang_stok' => $stok+$qty,
            );
        $where = array(
            'barang_id' => $barang_id,
            'gudang_id' => $gudang_id
        );

        $this->mdl_gudang->update_data($where,$data,'tbl_gudang_detail');
        // $this->mdl_barang->update_data($where,$data,'tbl_barang');
        // $this->mdl_inventory->input_data($data2,'tbl_inventory');

        $this->session->set_flashdata('berhasil','Stok Telah Di Tambah');
        redirect('gudang','refresh'); 
	}

    function ambil_data(){

        $id=$this->input->post('id');
        $mod=$this->input->post('mod');
        if ($mod == 'gudang') {
            $data = $this->mdl_gudang->get_data_barang($id);
            $opt = '<option value="0">--Pilih Barang--</option>';
            foreach ($data as $d) {
                $opt .= '<option value="'.$d->barang_id.'">'.$d->barang_nama.'</option>';
            }

            echo $opt;
        }elseif($mod == 'barang'){
            $idg=$this->input->post('idg');
            $data = $this->mdl_gudang->get_row_barang($id,$idg);
            echo $data->gudang_stok;
        }
        
        
    }

}
