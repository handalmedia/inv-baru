<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->auth->restrict();
        $this->load->library('session');
        $this->load->model('mdl_gudang');
        $this->load->model('mdl_barang');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$jumlah_data = $this->mdl_barang->jml_data();
        $this->load->library('pagination');
        $config['base_url'] = base_url('barang/index');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 20;
        $from = $this->uri->segment(4);
        $this->pagination->initialize($config);
        $data['barang'] = $this->mdl_barang->data($config['per_page'],$from);

		//$data['hotel'] = $this->mdl_masterhotel->get_data();
		$this->load->view('backend/header');
		$this->load->view('backend/barang/index',$data);
		$this->load->view('backend/footer');
	}

	public function simpan()
	{


        $nama = $this->input->post('nama');
        $harga = $this->input->post('harga');
        
        $data = array(
            'barang_nama' => $nama,
            'barang_stok' => 0,
            'barang_harga' => $harga
            );
        $this->mdl_barang->input_data($data,'tbl_barang');
        $this->session->set_flashdata('berhasil','Data Berhasil Di Tambahkan');
        redirect('barang','refresh');     

		
	}

	public function ubah(){
		$id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $harga = $this->input->post('harga');

        $data = array(
            'barang_nama' => $nama,
            'barang_harga' => $harga
            );
        $where = array(
            'barang_id' => $id
        );
        $this->mdl_barang->update_data($where,$data,'tbl_barang');
        $this->session->set_flashdata('berhasil','Data Berhasil Di Ubah');
        redirect('barang','refresh'); 
	}

    public function hapus($id){
        $where = array(
            'barang_id' => $id
        );
        $del = $this->mdl_barang->hapus_data($where,'tbl_barang');
        if($del){
            $this->session->set_flashdata('berhasil','Data Berhasil Di Hapus');
        }else{
            $this->session->set_flashdata('gagal','Data Tidak Dapat Di Hapus');
        }
        redirect('barang','refresh'); 

    }
}
