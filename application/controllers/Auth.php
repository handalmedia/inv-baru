<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('mdl_auth');
	}

	public function index()
	{
		$this->load->view('login_view');
		// $this->load->view('backend/header');
		// $this->load->view('backend/dashboard');
		// $this->load->view('backend/footer');
	}


	function login() {
		$user = $this->input->post('username');
		$pass = $this->input->post('password');
		$user = $this->mdl_auth->log_author($user,$pass);
			if($user==true){
				$newdata = array(
						        'username'  => $user->author_username,
						        'password'     => $user->author_password
						   );
				$this->session->set_userdata($newdata);
				// $data['hasil']=1;
				// echo json_encode($data);
				redirect('home', 'refresh');
				//print_r($newdata);
			}else{
				// $data['hasil'] = 0;
				// echo json_encode($data);

				echo "<script>alert('Username Dan Password Tidak Cocok Dengan Data Manapun'); window.location='".base_url()"';</script>";
				
			}
	}

	function logout(){
		//helper_log("logout", "Logout");
		$this->session->sess_destroy();
		redirect('backdoor','refresh');
    }

}
