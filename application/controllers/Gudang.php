<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->auth->restrict();
        $this->load->library('session');
        $this->load->model('mdl_gudang');
        $this->load->model('mdl_barang');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$jumlah_data = $this->mdl_gudang->jml_data();
        $this->load->library('pagination');
        $config['base_url'] = base_url('gudang/index');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 20;
        $from = $this->uri->segment(4);
        $this->pagination->initialize($config);
        $data['gudang'] = $this->mdl_gudang->data($config['per_page'],$from);

		//$data['hotel'] = $this->mdl_masterhotel->get_data();
		$this->load->view('backend/header');
		$this->load->view('backend/gudang/index',$data);
		$this->load->view('backend/footer');
	}

	public function simpan()
	{


        $nama = $this->input->post('nama');
        
        $data = array(
            'gudang_nama' => $nama,
            );
        $this->mdl_gudang->input_data($data,'tbl_gudang');
        $this->session->set_flashdata('berhasil','Data Berhasil Di Tambahkan');
        redirect('gudang','refresh');     

		
	}

	public function ubah(){
		$id = $this->input->post('id');
        $nama = $this->input->post('nama');

        $data = array(
            'gudang_nama' => $nama
            );
        $where = array(
            'gudang_id' => $id
        );
        $this->mdl_gudang->update_data($where,$data,'tbl_gudang');
        $this->session->set_flashdata('berhasil','Data Berhasil Di Ubah');
        redirect('gudang','refresh'); 
	}

    public function hapus($id){
        $where = array(
            'gudang_id' => $id
        );
        $del = $this->mdl_gudang->hapus_data($where,'tbl_gudang');

        if($del){
            $this->session->set_flashdata('berhasil','Data Berhasil Di Hapus');
        }else{
            $this->session->set_flashdata('gagal','Data Tidak Dapat Di Hapus');
        }

        redirect('gudang','refresh'); 

    }

    function gudang_detail($id){

        $data['detail'] = $this->mdl_gudang->get_detail($id);
        $data['barang'] = $this->mdl_barang->get_data_exception($id);
        $data['gudang_id'] = $id;
        $this->load->view('backend/header');
        $this->load->view('backend/gudang/detail_gudang',$data);
        $this->load->view('backend/footer');
    }

    function simpan_detail($id){

        $idbarang = $this->input->post('barang_id');
        $data = array(
            'gudang_id' => $id,
            'barang_id' => $idbarang,
            'gudang_stok' => 0
            );
        $this->mdl_gudang->input_data($data,'tbl_gudang_detail');
        $this->session->set_flashdata('berhasil','Data Berhasil Di Tambahkan');
        redirect('gudang/gudang_detail/'.$id,'refresh');     
    }

    function hapus_detail($id,$idgudang){

        $cek = $this->mdl_gudang->get_row($id);
        
        if($cek->gudang_stok == 0){
            $where = array(
                'gudang_detail_id' => $id
            );
            $this->mdl_gudang->hapus_data($where,'tbl_gudang_detail');
            $this->session->set_flashdata('berhasil','Data Berhasil Di Hapus');
        }else{
            $this->session->set_flashdata('gagal','Data Tidak Dapat Di Hapus Karena Masih Ada Stok');
        }
        redirect('gudang/gudang_detail/'.$idgudang,'refresh');     

    }
        
}
