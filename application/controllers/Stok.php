<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends CI_Controller {

	function __construct() {

        parent::__construct();
        $this->auth->restrict();
        $this->load->library('session');
        //$this->load->model('mdl_stok');
        $this->load->model('mdl_barang');
        $this->load->model('mdl_inventory');
        $this->load->model('mdl_gudang');
        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$data['barang'] = $this->mdl_barang->get_data();
        $data['jenis_id'] = 2;
        $data['gudang'] = $this->mdl_gudang->get_data();
		$this->load->view('backend/header');
		$this->load->view('backend/barang/in_stok',$data);
		$this->load->view('backend/footer');
	}


	public function instok(){
        $jenis_id = $this->input->post('jenis_id');
        $barang_id = $this->input->post('barang_id');
        $brg_id = $this->input->post('brg_id');
        $gudang_id = $this->input->post('gudang_id');
        $gudang = $this->input->post('gudang');
		$stokgdg = $this->input->post('stokgdg');
        $qty = $this->input->post('qty');

        if($gudang=='1'){
            $ket = 'Dari Gudang';
            $barang_id = $brg_id;
            $stok = $this->mdl_barang->get_stok($barang_id);
        }else{
            $stok = $this->input->post('stok');
            $ket = 'Dari Luar Gudang';
        }

        //update stok tbl barang
        $data = array(
            'barang_stok' => $stok+$qty,
            );
        $where = array(
            'barang_id' => $barang_id
        );

        $this->mdl_barang->update_data($where,$data,'tbl_barang');

        if($gudang == '1'){
            //update stok gudang
            $datagudang = array(
                            'gudang_stok' => $stokgdg - $qty
                        );
            $wheregdg = array(
                            'gudang_id' => $gudang_id,
                            'barang_id' => $brg_id
                        );
            $this->mdl_gudang->update_data($wheregdg,$datagudang,'tbl_gudang_detail');
        }
        //inventory
        $data2 = array(
            'jenis_id' => $jenis_id,
            'barang_id' => $barang_id,
            'inv_qty' => $qty,
            'keterangan' => $ket
            );        
        $this->mdl_inventory->input_data($data2,'tbl_inventory');

        $this->session->set_flashdata('berhasil','Stok Telah Di Tambah');
        redirect('stok','refresh'); 
	}

    function ambil_data(){

        $id=$this->input->post('id');
        $mod=$this->input->post('mod');
        if ($mod=='barang') {
            echo $this->mdl_barang->get_stok($id);
        }elseif($mod=='gudang'){

            $data = $this->mdl_gudang->get_data_barang($id);
            $opt = '<option value="0">--Pilih Barang--</option>';
            foreach ($data as $d) {
                $opt .= '<option value="'.$d->barang_id.'">'.$d->barang_nama.'</option>';
            }

            echo $opt;
        }elseif($mod=='brg'){
            $idgudang=$this->input->post('gdg');
            $data = $this->mdl_gudang->get_row_barang($id,$idgudang);
            echo $data->gudang_stok;
        }
        
        
        
    }

}
