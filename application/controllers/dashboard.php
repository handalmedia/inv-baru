<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	function __construct(){
		parent::__construct();
		//$this->load->model('Auth_model');
		$this->load->model('mdl_dashboard');
		$this->auth->restrict();
		$this->load->library('session');
	}

	public function index()
	{
		
		$data['favorit'] = $this->mdl_dashboard->get_fav(5);
		$data['jml_barang'] = $this->mdl_dashboard->get_data('tbl_barang')->num_rows();
		$data['jml_tx'] = $this->mdl_dashboard->get_data('tbl_tx')->num_rows();
		
		$data['menipis'] = $this->mdl_dashboard->get_menipis('tbl_barang',10)->num_rows();
		$data['listmenipis'] = $this->mdl_dashboard->get_menipis('tbl_barang',10)->result();

		$data['total'] = $this->mdl_dashboard->total_jual(date('m'));
		//laporan
		$data['masuk'] = $this->mdl_dashboard->get_masuk(date('m'),NULL,10);
		$data['keluar'] = $this->mdl_dashboard->get_keluar(date('m'),NULL,10);
		$data['jual'] = $this->mdl_dashboard->get_jual(date('m'),NULL,10);

		$data['mharian'] = $this->mdl_dashboard->get_mharian(10);
		$data['jharian'] = $this->mdl_dashboard->get_jharian(10);


		$this->load->view('backend/header');
		$this->load->view('backend/dashboard',$data);
		$this->load->view('backend/footer');
	}

	function print_masuk($bulan,$tahun){
		$data['listbulan'] = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$th = $this->mdl_dashboard->get_alltahun();
		$data['allth'] = $th;
		if($tahun == '0'){$tahun = NULL;};
		if($bulan == '0'){$bulan = NULL;};
		
		if($bulan !=NULL){
			$data['data'] = $this->mdl_dashboard->get_masuk($bulan,$tahun,NULL);
		}elseif($tahun != NULL){
			for($i = 1; $i<=12;$i++){
				if(strlen($i) == 1){
					$data['data'.$i] = $this->mdl_dashboard->get_masuk('0'.$i,$tahun,NULL);
				}else{
					$data['data'.$i] = $this->mdl_dashboard->get_masuk($i,$tahun,NULL);
				}
			}
		}else{
			foreach ($th as $t) {
				for($i = 1; $i<=12;$i++){
					if(strlen($i) == 1){
						$data['data'.$i][$t->Th] = $this->mdl_dashboard->get_masuk('0'.$i,$t->Th,NULL);
					}else{
						$data['data'.$i][$t->Th] = $this->mdl_dashboard->get_masuk($i,$t->Th,NULL);
					}
				}
			}
		}
		

		$data['bulan'] = $bulan;		
		$data['tahun'] = $tahun;		
		$this->load->view('backend/laporan/print_masuk',$data);
	}

	function print_penjualan($bulan,$tahun){
		$data['listbulan'] = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
		$th = $this->mdl_dashboard->get_alltahun();
		$data['allth'] = $th;
		if($tahun == '0'){$tahun = NULL;};
		
		if($bulan == '0'){$bulan = NULL;};
		
		if($bulan !=NULL){
			$data['data'] = $this->mdl_dashboard->get_penjualan($bulan,$tahun);
		}elseif($tahun != NULL){
			for($i = 1; $i<=12;$i++){
				if(strlen($i) == 1){
					$data['data'.$i] = $this->mdl_dashboard->get_penjualan('0'.$i,$tahun);
				}else{
					$data['data'.$i] = $this->mdl_dashboard->get_penjualan($i,$tahun);
				}
			}
		}else{
			foreach ($th as $t) {
				for($i = 1; $i<=12;$i++){
					if(strlen($i) == 1){
						$data['data'.$i][$t->Th] = $this->mdl_dashboard->get_penjualan('0'.$i,$t->Th);
					}else{
						$data['data'.$i][$t->Th] = $this->mdl_dashboard->get_penjualan($i,$t->Th);
					}
				}
			}
		}
			

		$data['bulan'] = $bulan;		
		$data['tahun'] = $tahun;		
		$this->load->view('backend/laporan/print_penjualan',$data);
	}

	function print_mharian(){
		$data['list'] = $this->mdl_dashboard->get_mharian(NULL);
		$this->load->view('backend/laporan/print_mharian',$data);
	}

	function print_jharian(){
		$data['list'] = $this->mdl_dashboard->get_jharian(NULL);
		$this->load->view('backend/laporan/print_jharian',$data);
	}
}
