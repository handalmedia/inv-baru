-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Des 2018 pada 09.06
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inv_baru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `barang_id` int(11) NOT NULL,
  `barang_nama` varchar(255) NOT NULL,
  `barang_harga` int(11) NOT NULL,
  `barang_stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_gudang`
--

CREATE TABLE `tbl_gudang` (
  `gudang_id` int(11) NOT NULL,
  `gudang_nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_gudang_detail`
--

CREATE TABLE `tbl_gudang_detail` (
  `gudang_detail_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `gudang_stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inventory`
--

CREATE TABLE `tbl_inventory` (
  `inv_id` int(11) NOT NULL,
  `inv_tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jenis_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_jenis_tx`
--

CREATE TABLE `tbl_jenis_tx` (
  `jenis_id` int(11) NOT NULL,
  `jenis_nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_jenis_tx`
--

INSERT INTO `tbl_jenis_tx` (`jenis_id`, `jenis_nama`) VALUES
(1, 'penjualan'),
(2, 'stok masuk'),
(3, 'stok keluar'),
(4, 'Mutasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tx`
--

CREATE TABLE `tbl_tx` (
  `tx_id` varchar(20) NOT NULL,
  `tx_tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tx_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tx_detail`
--

CREATE TABLE `tbl_tx_detail` (
  `tx_detail_id` int(11) NOT NULL,
  `tx_id` varchar(20) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `tx_detail_qty` int(11) NOT NULL,
  `tx_detail_sub` int(11) NOT NULL,
  `tx_detail_ket` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`) VALUES
(1, 'superadmin', 'ac497cfaba23c4184cb03b97e8c51e0a', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`barang_id`);

--
-- Indeks untuk tabel `tbl_gudang`
--
ALTER TABLE `tbl_gudang`
  ADD PRIMARY KEY (`gudang_id`);

--
-- Indeks untuk tabel `tbl_gudang_detail`
--
ALTER TABLE `tbl_gudang_detail`
  ADD PRIMARY KEY (`gudang_detail_id`),
  ADD KEY `barang_id` (`barang_id`) USING BTREE,
  ADD KEY `gudang_id` (`gudang_id`) USING BTREE;

--
-- Indeks untuk tabel `tbl_inventory`
--
ALTER TABLE `tbl_inventory`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `barang_rel2` (`barang_id`),
  ADD KEY `jenis_rl` (`jenis_id`);

--
-- Indeks untuk tabel `tbl_jenis_tx`
--
ALTER TABLE `tbl_jenis_tx`
  ADD PRIMARY KEY (`jenis_id`);

--
-- Indeks untuk tabel `tbl_tx`
--
ALTER TABLE `tbl_tx`
  ADD PRIMARY KEY (`tx_id`);

--
-- Indeks untuk tabel `tbl_tx_detail`
--
ALTER TABLE `tbl_tx_detail`
  ADD PRIMARY KEY (`tx_detail_id`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `tx_id` (`tx_id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `barang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_gudang`
--
ALTER TABLE `tbl_gudang`
  MODIFY `gudang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_gudang_detail`
--
ALTER TABLE `tbl_gudang_detail`
  MODIFY `gudang_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_inventory`
--
ALTER TABLE `tbl_inventory`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_jenis_tx`
--
ALTER TABLE `tbl_jenis_tx`
  MODIFY `jenis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_tx_detail`
--
ALTER TABLE `tbl_tx_detail`
  MODIFY `tx_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_gudang_detail`
--
ALTER TABLE `tbl_gudang_detail`
  ADD CONSTRAINT `barangrel` FOREIGN KEY (`barang_id`) REFERENCES `tbl_barang` (`barang_id`),
  ADD CONSTRAINT `gudangrel` FOREIGN KEY (`gudang_id`) REFERENCES `tbl_gudang` (`gudang_id`);

--
-- Ketidakleluasaan untuk tabel `tbl_inventory`
--
ALTER TABLE `tbl_inventory`
  ADD CONSTRAINT `barang_rel2` FOREIGN KEY (`barang_id`) REFERENCES `tbl_barang` (`barang_id`),
  ADD CONSTRAINT `jenis_rl` FOREIGN KEY (`jenis_id`) REFERENCES `tbl_jenis_tx` (`jenis_id`);

--
-- Ketidakleluasaan untuk tabel `tbl_tx_detail`
--
ALTER TABLE `tbl_tx_detail`
  ADD CONSTRAINT `barang_rel` FOREIGN KEY (`barang_id`) REFERENCES `tbl_barang` (`barang_id`),
  ADD CONSTRAINT `txrel` FOREIGN KEY (`tx_id`) REFERENCES `tbl_tx` (`tx_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
